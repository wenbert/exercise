## The Exercise
The exercise
Your mission, should you choose to accept it, is to write a Laravel application that helps a user understand how much quantity of a product is available for use.
The application should display an interface with a button and a single input that represents the requested quantity of a product.

When the button is clicked, the interface should show either the $ value of the quantity of that product that will be applied, or an error message if the quantity to be applied exceeds the quantity on hand.

Note that product purchased first should be used first, therefore the quantity on hand should be the most recently purchased.

A csv file is attached that you should use as your data source.

Here is a small example of inventory movements:
a. Purchased 1 unit at $10 per unit
b. Purchased 2 units at $20 per unit
c. Purchased 2 units at $15 per unit
d. Applied 2 units

After the 2 units have been applied, the purchased units in 'a' have been completely used up. Only 1 unit from 'b' has been used, so the remaining inventory looks like this:

b. 1 unit at $20 per unit c. 2 units at $15 per unit
Quantity on hand = 3 Valuation = (1 * 20) + (2 * 15) = $50

Here's what we'll be looking for in your submission:

* Breaking the code down into logical classes and methods that have a single responsibility
* Clear comments that explain the code logic
* Descriptive method and variable names
* Usage of suitable packages (if applicable) to solve a problem rather than writing code from scratch. No need to reinvent the wheel :)
* A descriptive README file
* Some PHPUnit tests to ensure other developers don't accidentally break

### The CSV file
```csv
Date,Type,Quantity,Unit Price
05/06/2020,Purchase,10,5
07/06/2020,Purchase,30,4.5
08/06/2020,Application,-20,
09/06/2020,Purchase,10,5
10/06/2020,Purchase,34,4.5
15/06/2020,Application,-25,
23/06/2020,Application,-37,
10/07/2020,Purchase,47,4.3
12/07/2020,Application,-38,
13/07/2020,Purchase,10,5
25/07/2020,Purchase,50,4.2
26/07/2020,Application,-28,
31/07/2020,Purchase,10,5
14/08/2020,Purchase,15,5
17/08/2020,Purchase,3,6
29/08/2020,Purchase,2,7
31/08/2020,Application,-30,
```

## Initial Steps
Checkout this repository.

Install:
```
composer install
or
composer install --ignore-platform-reqs
```

Run:
```
./vendor/bin/sail up
```

Copy `.env.example` into a new `.env` file.

Next, we run the migrations with:
```
php artisan migrate
```

I left the default migrations that came with the Laravel installation.

* Migration for the `inventories` table which contain the different types (`Application` and `Purchase`).
* And a migration for the `transactions` table. This table contains the `Application` details.
    * ie: If we had an `Application` we store the ID of the `Purchase` linked to it.
* A migration to insert data into `inventories` table - based on the provided CSV file.
* A migration to insert data into `transactions` table.

The migrations will insert data based on the CSV file above.

## Table Relationships
The `inventory` table has many `transactions` - linked through a custom field called `application_id`.
At the moment, we only save transactions if it is `Application`.

Using `php artisan tinker`, we should see something like this:
```
>>> $inventory = new App\Models\Inventory();
=> App\Models\Inventory {#4223}
>>> $inventory->find(3);
=> App\Models\Inventory {#3500
     id: 3,
     quantity: -20,
     quantity_remaining: null,
     unit_price: null,
     type: "Application",
     created_at: "2020-06-08 00:00:00",
     updated_at: "2020-06-08 00:00:00",
   }
>>> $inventory->find(3)->transactions;
=> Illuminate\Database\Eloquent\Collection {#4441
     all: [
       App\Models\Transaction {#4440
         id: 1,
         application_id: 3,
         purchase_id: 1,
         quantity: 10,
         unit_price: "5.00",
         created_at: "2020-06-08 00:00:00",
         updated_at: "2020-06-08 00:00:00",
       },
       App\Models\Transaction {#3511
         id: 2,
         application_id: 3,
         purchase_id: 2,
         quantity: 10,
         unit_price: "4.50",
         created_at: "2020-06-08 00:00:00",
         updated_at: "2020-06-08 00:00:00",
       },
     ],
   }
>>>
```
From the resulting transactions, we know that the line in the CSV file: `08/06/2020,Application,-20,`
took 20 items from `inventory.id`'s `1,2` for `10` qty each.
The `transaction.purchase_id` is a reference to the "original" item from the `inventory` table.

## The form
Open up: `http://localhost`. You should see a form with a single textbox and a button.
It should also display the current quantity on hand and the current valuation of the quantity.
Like:
```
Quantity on Hand: 43 Valuation: $211.60
```

Put in `3` in the textbox and Submit. You should see a message that looks like:
```
Application was successful! Quantity Applied: 3 Value: $12.60
```

Entering a value that is more than the current quantity on hand would display:
```
Not enough quantity remaining! Cannot apply: 41
```

Entering 0 or something smaller would display:
```
Quantity cannot be zero or a negative value!
```

Refreshing the form will not resubmit the POST data.

## Unit Test
I added `.env.testing` with contents that look like this:
```
APP_NAME=Laravel
APP_ENV=testing
APP_KEY=base64:r3JHTJt9cITlnqIVj7dd7jRxfRqldC/UEMwWJg+dkxw=

DB_CONNECTION=sqlite
DB_DATABASE=:memory:
```

You can run the unit tests in PHPStorm or through the CLI:
```
php artisan test --filter InventoryServiceTest

   PASS  Tests\Unit\Services\InventoryServiceTest
  ✓ get quantity data with some quantity remaining
  ✓ get quantity data with no quantity remaining
  ✓ get quantity data with next quantity
  ✓ apply quantity no quantity
  ✓ apply quantity no enough quantity
  ✓ apply quantity

  Tests:  6 passed
  Time:   5.02s
```

## Relevant Files
* The Controller `app/Http/Controllers/InventoryController.php`
* The View `resources/views/inventory/form.blade.php`
* The Service `app/Services/InventoryService.php`
* The Models:
  * `app/Models/Inventory.php`
  * `app/Models/Transaction.php`
* The Test `tests/Unit/Services/InventoryServiceTest.php`
* And the migration files


