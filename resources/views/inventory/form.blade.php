<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: 'Nunito', sans-serif;
            text-align: center;
        }

        .message-container {
            margin: 15px auto;
        }

        .alert {
            border: 1px solid green;
            margin: 2px auto;
            padding: 2px;
            color: green;
            width: 300px;
        }

        .error {
            border: 1px solid red;
            margin: 2px auto;
            padding: 2px;
            color: red;
            width: 300px;
        }

        form label {
            font-size: 18px;
        }

        form .quantity-input {
            border: 1px solid #1d2f54;
            font-size: 18px;
            text-align: right;
            width: 60px;
        }

        form button {
            font-size: 18px;
        }
    </style>
</head>
<body class="antialiased">
<div>
    <h1>Fertiliser Inventory</h1>

    @if (session('status'))
        <div class="message-container">
            <div class="alert">
                {{ session('status') }}
            </div>
        </div>
    @endif
    @if (session('error'))
        <div class="message-container">
            <div class="error">
                {{ session('error') }}
            </div>
        </div>
    @endif

    <form name="form" method="post" action="{{url('/applyquantity')}}">
        @csrf
        <div>
            <label for="quantity">Quantity to Apply: </label>
            <input class="quantity-input" type="number" max="1000" id="title" name="quantityToApply" required="" value="">
        </div>
        <button type="submit">Submit</button>
    </form>
</div>
<h2>Quantity on Hand: <?php echo $quantity_on_hand ?> Valuation: <?php echo $valuation ?>
</body>
</html>
