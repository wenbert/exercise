<?php

use App\Http\Controllers\InventoryController;
use Illuminate\Support\Facades\Route;

Route::get('/', [InventoryController::class, 'index'])->name('inventory.form');
Route::post( '/applyquantity', [InventoryController::class, 'applyQuantity'])->name('apply.quantity');
