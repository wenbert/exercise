<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inventory extends Model
{
    use HasFactory;

    public const TYPE_APPLICATION = 'Application';
    public const TYPE_PURCHASE = 'Purchase';

    protected $fillable = ['type', 'quantity'];

    public function getTotalQuantityRemaining(): int
    {
        return (int) self::where('type', self::TYPE_PURCHASE)->sum('quantity_remaining');
    }

    public function getTotalValuationOfQuantityRemaining()
    {
        return self::where('type', self::TYPE_PURCHASE)->sum(DB::raw('quantity_remaining * unit_price'));
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'application_id');
    }

    public function getOldestItemWithRemainingQuantity(): Inventory
    {
        return self::select('id', 'quantity', 'quantity_remaining', 'unit_price')
            ->where('quantity_remaining', '>', 0)
            ->where('type', self::TYPE_PURCHASE)
            ->orderBy('created_at', 'ASC')
            ->first();
    }
}
