<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['application_id', 'purchase_id', 'quantity','unit_price'];

    public function application()
    {
        return $this->belongsTo(Inventory::class, 'application_id');
    }
}
