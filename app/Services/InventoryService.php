<?php

namespace App\Services;

use App\Models\Inventory;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class InventoryService
{
    /**
     * @var Inventory
     */
    private $inventory;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * An array that contains the transaction details to be saved into the transaction table.
     * A transaction detail contains the "purchase id" (many) and the "application id" (one).
     *
     * @var array
     */
    private $transactionDetails = [];

    public function __construct(Inventory $inventory, Transaction $transaction)
    {
        $this->inventory = $inventory;
        $this->transaction = $transaction;
    }

    /**
     * Apply the quantity to the oldest item in the inventory.
     *
     * @param int $quantityToApply
     * @return array
     * @throws \Throwable
     */
    public function applyQuantity(int $quantityToApply = 0): array
    {
        $originalQuantity = $quantityToApply;
        if ($quantityToApply <= 0) {
            return [];
        }

        $quantityOnHand = $this->inventory->getTotalQuantityRemaining();
        if ($quantityToApply > $quantityOnHand) {
            return [];
        }

        $quantityCountdown = $quantityToApply;
        try {
            DB::beginTransaction();

            do {
                $item = $this->getItemWithRemainingQuantity();

                $quantityData = $this->getQuantityDataAndPrepareTransactions($item, $quantityToApply);
                $item->quantity_remaining = $quantityData['quantity_remaining'];
                $item->save();

                $quantityToApply = $quantityData['next_quantity'];
                $quantityCountdown -= $quantityData['quantity_for_countdown'];

            // Continue to deduct from quantity as long as we have `next_quantity` or the countdown is still greater than 0
            } while ($quantityCountdown > 0 && $quantityToApply);

            $inventoryApplicationId = $this->createInventoryApplication($originalQuantity);
            $transactions = $this->createTransactions($inventoryApplicationId->id, $this->getTransactionDetails());

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        return $transactions;
    }

    /**
     * Create the Application line in the Inventory
     *
     * @param int $quantity
     * @return int
     */
    protected function createInventoryApplication(int $quantity): Inventory
    {
        $inventoryApplication = $this->inventory->create(
            [
                'type' => Inventory::TYPE_APPLICATION,
                'quantity' => $quantity * -1,
            ]
        );

        return $inventoryApplication;
    }

    /**
     * Create Transactions for the Application. Returns an array of summary data
     *
     * @param int $inventoryApplicationId
     * @param array $transactions
     * @return array
     */
    protected function createTransactions(int $inventoryApplicationId, array $transactions): array
    {
        $summary = [
            'total_quantity_to_apply' => 0,
            'total_value_of_quantity' => 0.0,
        ];
        foreach ($transactions as $id => $applicationData) {
            $this->transaction->create(
                [
                    'purchase_id' => $id,
                    'application_id' => $inventoryApplicationId,
                    'quantity' => $applicationData['quantity'],
                    'unit_price' => $applicationData['unit_price'],
                ]
            );
            $summary['total_quantity_to_apply'] += $applicationData['quantity'];
            $summary['total_value_of_quantity'] += $applicationData['quantity'] * $applicationData['unit_price'];
        }
        return $summary;
    }

    /**
     * Get quantity data. ie: Quantity to be deducted for next iteration, quantity_remaining, etc.
     * This method also prepares the array to be used when creating the transaction records.
     *
     * @param Inventory $item
     * @param int $quantityToApply
     * @return array
     */
    protected function getQuantityDataAndPrepareTransactions(Inventory $item, int $quantityToApply): array
    {
        $quantityRemaining = $item->quantity_remaining;

        if (($quantityRemaining - $quantityToApply) === 0) {
            $this->transactionDetails[$item->id] = [
                'quantity' => $quantityToApply,
                'unit_price' => $item->unit_price,
            ];

            return [
                'quantity_remaining' => 0,
                'quantity_for_countdown' => $quantityToApply,
                'next_quantity' => 0,
            ];
        }

        if ($quantityToApply < $quantityRemaining) {
            $this->transactionDetails[$item->id] = [
                'quantity' => $quantityToApply,
                'unit_price' => $item->unit_price,
            ];

            return [
                'quantity_remaining' => $item->quantity_remaining - $quantityToApply,
                'quantity_for_countdown' => $quantityToApply,
                'next_quantity' => 0
            ];
        }

        if (($quantityRemaining - $quantityToApply) < 0) {
            $quantityToApply = abs($quantityRemaining - $quantityToApply);
            $this->transactionDetails[$item->id] = [
                'quantity' => $quantityRemaining,
                'unit_price' => $item->unit_price,
            ];

            return [
                'quantity_remaining' => 0,
                'quantity_for_countdown' => $quantityRemaining,
                'next_quantity' => $quantityToApply
            ];
        }

        return [];
    }

    public function getItemWithRemainingQuantity(): Inventory
    {
        return $this->inventory->getOldestItemWithRemainingQuantity();
    }

    public function getTransactionDetails(): array
    {
        return $this->transactionDetails;
    }

    public function getQuantityOnHand(): int
    {
        return $this->inventory->getTotalQuantityRemaining();
    }

    public function getValuation()
    {
        return $this->inventory->getTotalValuationOfQuantityRemaining();
    }
}