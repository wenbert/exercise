<?php

namespace App\Http\Controllers;

use App\Services\InventoryService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Throwable;

class InventoryController extends Controller
{
    /**
     * @var InventoryService
     */
    private $inventoryService;

    public function __construct(InventoryService $inventoryService)
    {
        $this->inventoryService = $inventoryService;
    }

    /**
     * Display the form with the single input and button.
     *
     * @return View
     */
    public function index(): View
    {
        $valuation = '$' . number_format($this->inventoryService->getValuation(), 2);
        $quantityOnHand = $this->inventoryService->getQuantityOnHand();
        return view(
            'inventory.form',
            [
                'quantity_on_hand' => $quantityOnHand,
                'valuation' => $valuation,
            ]
        );
    }

    /**
     * The action that handles the POST request from the form.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws Throwable
     */
    public function applyQuantity(Request $request): RedirectResponse
    {
        $quantityToApply = (int) ($request->quantityToApply ?? 0);
        if ($quantityToApply <= 0) {
            return Redirect::route('inventory.form')->with('error', 'Quantity cannot be zero or a negative value!');
        }

        $result = $this->inventoryService->applyQuantity($quantityToApply);

        if (empty($result)) {
            return Redirect::route('inventory.form')
                ->with('error', 'Not enough quantity remaining! Cannot apply: ' . $quantityToApply);
        }

        $message = 'Application was successful! ' .
            'Quantity Applied: ' . $result['total_quantity_to_apply'] . ' ' .
            'Value: $' . number_format($result['total_value_of_quantity'], 2);

        return Redirect::route('inventory.form')->with('status', $message);
    }
}
