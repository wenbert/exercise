<?php

namespace Tests\Unit\Services;

use App\Models\Inventory;
use App\Models\Transaction;
use App\Services\InventoryService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use ReflectionClass;
use Tests\TestCase;

class InventoryServiceTest extends TestCase
{
    use RefreshDatabase;

    public static function callMethod($obj, $name, array $args)
    {
        $class = new ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }

    public function testGetQuantityDataWithSomeQuantityRemaining()
    {
        $inventory = new Inventory();
        $inventory->quantity = 9;
        $inventory->quantity_remaining = 9;
        $inventory->unit_price = 10.00;

        $service = new InventoryService($inventory, $this->createMock(Transaction::class));

        $result = self::callMethod(
            $service,
            'getQuantityDataAndPrepareTransactions',
            array($inventory, 4)
        );

        $this->assertEquals(
            [
                "quantity_remaining" => 5,
                "quantity_for_countdown" => 4,
                "next_quantity" => 0,
            ],
            $result
        );
    }

    /**
     * Test for current quantity equal to the quantity to apply.
     * If the item has quantity = 5, we want to remove 5, then we
     * expect that the quantity_remaining is 0.
     */
    public function testGetQuantityDataWithNoQuantityRemaining()
    {
        $inventory = new Inventory();
        $inventory->quantity = 5;
        $inventory->quantity_remaining = 5;
        $inventory->unit_price = 10.00;

        $service = new InventoryService($inventory, $this->createMock(Transaction::class));

        $result = self::callMethod(
            $service,
            'getQuantityDataAndPrepareTransactions',
            array($inventory, 5)
        );

        $this->assertEquals(
            [
                'quantity_remaining' => 0,
                'quantity_for_countdown' => 5,
                'next_quantity' => 0,
            ],
            $result
        );
    }

    /**
     * Test when the current item is less than the quantity we want to remove.
     * For example, current item quantity is 5, but we want to remove 8.
     * We expect that there is a remaining quanity of 3 (to be deducted from next item).
     */
    public function testGetQuantityDataWithNextQuantity()
    {
        $inventory = new Inventory();
        $inventory->quantity = 5;
        $inventory->quantity_remaining = 5;
        $inventory->unit_price = 10.00;

        $service = new InventoryService($inventory, $this->createMock(Transaction::class));

        $result = self::callMethod(
            $service,
            'getQuantityDataAndPrepareTransactions',
            array($inventory, 8)
        );

        $this->assertEquals(
            [
                'quantity_remaining' => 0,
                'quantity_for_countdown' => 5,
                'next_quantity' => 3,
            ],
            $result
        );
    }

    /**
     * Test when there is no more quantity left to remove.
     */
    public function testApplyQuantityNoQuantity()
    {
        $inventoryMock = $this->createMock(Inventory::class);
        $inventoryMock->expects($this->never())
            ->method('getTotalQuantityRemaining');

        $service = new InventoryService($inventoryMock, $this->createMock(Transaction::class));
        $result = $service->applyQuantity(0);
        $this->assertEmpty($result);
    }

    /**
     * Test when the quantity remaining is not enough.
     */
    public function testApplyQuantityNoEnoughQuantity()
    {
        $inventoryMock = $this->createMock(Inventory::class);
        $inventoryMock->expects($this->once())
            ->method('getTotalQuantityRemaining')
            ->willReturn(10);

        $service = new InventoryService($inventoryMock, $this->createMock(Transaction::class));
        $result = $service->applyQuantity(11);
        $this->assertEmpty($result);
    }

    /**
     * Test the applyQuantity method.
     * In here, we also check for the transactionDetails that will be saved
     * to the transactions table.
     */
    public function testApplyQuantity()
    {
        $inventory = new Inventory();
        $inventory->id = 99; // Will be reflected in the transactionDetails array
        $inventory->quantity = 5; // We will deduct 3. In the end, this will be 2.
        $inventory->quantity_remaining = 5;
        $inventory->unit_price = 10.00;

        $inventoryMock = $this->getMockBuilder(Inventory::class)
            ->disableOriginalConstructor()
            ->addMethods(['create'])
            ->onlyMethods(['getTotalQuantityRemaining', 'getOldestItemWithRemainingQuantity'])
            ->getMock();

        $inventoryMock->expects($this->atLeastOnce())
            ->method('getOldestItemWithRemainingQuantity')
            ->willReturn($inventory);

        $inventoryMock->expects($this->once())
            ->method('getTotalQuantityRemaining')
            ->willReturn(5);

        $newInventoryApplicationRecord = new Inventory();
        $newInventoryApplicationRecord->id = 2;
        $newInventoryApplicationRecord->quantity = -3;
        $newInventoryApplicationRecord->type = Inventory::TYPE_APPLICATION;

        $inventoryMock->expects($this->once())
            ->method('create')
            ->with(['type' => Inventory::TYPE_APPLICATION, 'quantity' => -3])
            ->willReturn($newInventoryApplicationRecord);

        $service = new InventoryService($inventoryMock, $this->createMock(Transaction::class));
        $result = $service->applyQuantity(3);
        $this->assertEquals(
            ['total_quantity_to_apply' => 3, 'total_value_of_quantity' => 30.00],
            $result
        );

        $transactionDetails = $service->getTransactionDetails();
        $this->assertEquals(
            [ 99 => ['quantity' => 3, 'unit_price' => 10]],
            $transactionDetails
        );

        $this->assertEquals(2, $inventory->quantity_remaining);
    }
}
